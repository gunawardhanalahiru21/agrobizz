<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Farmer;
use App\Customer;
use App\Transporter;

class APIController extends Controller {
    public function checkLogin(Request $request){

    }

    public function getAllUsers(Request $request){
        $farmer = Farmer::get();
        $customer = Customer::get();
        $transporter = Transporter::get();
        return response()->json(['message'=>'success','farmer'=>$farmer,'customer'=>$customer,'transporter'=>$transporter]);
    }
}
