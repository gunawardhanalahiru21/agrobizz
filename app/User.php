<?php

namespace App;

use App\Http\Middleware\isGovern;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    use softDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    const ACTIVE_TYPE = 'active';
    const ADMIN_TYPE = 'admin';
    const RDHS_TYPE = 'rdhs';
    const PHI_TYPE = 'phi';
    const NS1_TYPE = 'ns1';
    const Hospital_TYPE = 'hospital';
    const MOH_TYPE = 'moh';
    const GOVERN_TYPE = 'govern';

    //------------------------getting user active status----------------------------------
    public function isActive(){
        return $this->status === self::ACTIVE_TYPE;
    }
    // -----------------------middleware admin---------------------------------------------------------
    public function isAdmin(){
        return $this->role === self::ADMIN_TYPE;
    }
    // -----------------------middleware district office---------------------------------------------------------
    public function isRDHS(){
        return $this->role === self::RDHS_TYPE;
    }

    // -----------------------middleware district office---------------------------------------------------------
    public function isPHI(){
        return $this->role === self::PHI_TYPE;
    }
    // -----------------------middleware NS1---------------------------------------------------------
    public function isNS1(){
        return $this->role === self::NS1_TYPE;
    }
    //-----------------------middleware Hospital---------------------------------------------------------
    public function isHospital(){
        return $this->role === self::Hospital_TYPE;
    }

    //-----------------------middleware MOH---------------------------------------------------------
    public function isMOH(){
        return $this->role === self::MOH_TYPE;
    }

    //-----------------------middleware PHI WEB User---------------------------------------------------------
    public function isPHIWeb(){
        return $this->role === self::PHI_WEB_TYPE;
    }

    //-----------------------middleware Govern User---------------------------------------------------------
    public function isGovern(){
        return $this->role === self::GOVERN_TYPE;
    }

    //-------------- jobs (PHI)------------------
    public function jobs(){
        return $this->hasMany('App\PHIJob','user_id','id');
    }
    //-------------- Locations------------------
    public function locations(){
        return $this->hasMany(UserLocation::class,'user_id','id');
    }
    //-------------- Locations------------------
    public function userLocations(){
        return $this->hasMany(UserLocation::class,'user_id','id');
    }

    //-------------- Logs------------------
    public function logs(){
        return $this->hasMany(UserLog::class,'user_id','id');
    }

    //-------------- Last Known Location------------------
    public function lastKnownLocation(){
        return $this->hasMany(LocationRequest::class,'user_id','id');
    }

    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
