<?php

namespace App\Events;

use NotificationChannels\PusherPushNotifications\PusherChannel;
use NotificationChannels\PusherPushNotifications\PusherMessage;
use Illuminate\Notifications\Notification;

    class sendNotification extends Notification{
    public function via($notifiable){
        return [PusherChannel::class];
    }

    public function toPushNotification($notifiable){
        return PusherMessage::create()
        ->android()
        ->badge(1)
        ->sound('success')
        ->title($notifiable->title)
        ->body($notifiable->message)
        ->setOption("interests","debug-rans66")
        ->setOption("fcm","debug-rans66");
    }
 }
?>
