require('./bootstrap');
window.Vue = require('vue');
import Vue from 'vue';
import Vuetify from 'vuetify';
import '@mdi/font/css/materialdesignicons.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import '@fortawesome/fontawesome-free/css/all.css'
import 'vuetify/dist/vuetify.min.css';
import router from './router';
import * as VueGoogleMaps from 'vue2-google-maps';
import VueGraph from 'vue-graph';
import rcolor from 'rcolor';
import * as geolib from 'geolib';
import moment from 'moment'
import VueGeolocation from 'vue-browser-geolocation';

window.shapefile = require('shapefile');


Vue.use(geolib);
Vue.use(VueGeolocation);
Vue.use(rcolor);
Vue.use(Vuetify);
Vue.use(VueGraph);

Vue.prototype.moment = moment;
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyCfYeS7RPvYxbaeUyPc9pl95WAGiEm31eE',
        libraries: 'drawing,language=si,geometry,places,visualization',
    },
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

//-------Dashboard Main APP---------------------------------------------
Vue.component('mainapp', require('./components/mainapp.vue').default);

const app = new Vue({
    el: '#app',
    router,
    vuetify: new Vuetify(),
});
