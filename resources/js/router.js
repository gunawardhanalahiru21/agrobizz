import Vue from "vue";
import Router from "vue-router";
Vue.use(Router);

import mainappAdmin from "./components/mainapp";
import RegistrationPanel from "./components/registration.vue";
import LoginPanel from "./components/APILogin.vue";
import categoryPanel from "./components/category_view.vue";
import farmerRegistrationPanel from "./components/farmerRegistration.vue";
import AuthPanel from "./components/auth.vue";
import availableStockPanel from "./components/stock.vue";
import farmerProfilePanel from "./components/farmerProfile.vue";
import analysisPanel from "./components/analysis.vue";
import riceCategoryPanel from "./components/riceCategory.vue";

const routes = [{
        path: "/admin",
        component: mainappAdmin
    },
    {
        path: "/apilogin",
        name: "apilogin",
        component: LoginPanel
    },
    {
        path: "/Registration",
        name: "Registration",
        component: RegistrationPanel
    },
    {
        path: "/categorySelect",
        name: "categorySelect",
        component: categoryPanel
    },
    {
        path: "/farmerRegistration",
        name: "farmerRegistration",
        component: farmerRegistrationPanel
    },
    {
        path: "/authSet",
        name: "authSet",
        component: AuthPanel
    },
    {
        path: "/availableStockDetails",
        name: "availableStockDetails",
        component: availableStockPanel
    },
    {
        path: "/farmerProfile",
        name: "farmerProfile",
        component: farmerProfilePanel
    },
    {
        path: "/analysis",
        name: "analysis",
        component: analysisPanel
    },
    {
        path: "/riceCategory",
        name: "riceCategory",
        component: riceCategoryPanel
    },
];

export default new Router({
    mode: "history",
    routes
});
