import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
export const store = new Vuex.Store({
  state: {
    gmap:null,
    httpheader:{},
    mapInfoWindow:{
      infoPosition: null,
      infoContent: null,
      infoOpened: false,
      infoCurrentKey: null,
      infoOptions: null,
    },
    allFarmerDetails:[],
    allCustomerDetails:[],
    allTransporterDetails:[],
    earlyCirclePoint:[],
  },
  mutations: {
    setMap(state,payload){
      state.gmap = payload;
    },
    setInfoWindow(state,payload){
      state.mapInfoWindow = payload;
    },
    openInfoWindow(state,value){
      state.mapInfoWindow.infoOpened = value;
    },
    // ------------------------set Farmer Data---------------
    setAllFarmerDetails(state,payload){
      state.allFarmerDetails = payload;
    },
    // ------------------------set Customer Data---------------
    setAllCustomerDetails(state,payload){
      state.allCustomerDetails = payload;
    },
    // ------------------------set Transporter Data---------------
    setAllTransporterDetails(state,payload){
      state.allTransporterDetails = payload;
    },
    // ------------------------set earlyCirclePoint Data---------------
    setAllEarlyCirclePoint(state,payload){
      state.earlyCirclePoint = payload;
    },
  },
  getters: {
    gmap: state => state.gmap,
    mapInfoWindow: state => state.mapInfoWindow,
    allFarmerDetails: state => state.allFarmerDetails,
    allCustomerDetails: state => state.allCustomerDetails,
    allTransporterDetails: state => state.allTransporterDetails,
    earlyCirclePoint: state => state.earlyCirclePoint,
  }
});
