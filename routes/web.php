<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {return view('index');});
Route::get('apilogin', function () {return view('auth/login');});
Route::get('Registration', function () {return view('auth/register');});
Route::get('categorySelect', function () {return view('category/SelectCategory');});
Route::get('farmerRegistration', function () {return view('farmerReg/farmerRegistration');});
Route::get('authSet', function () {return view('auth/loginRegister');});
Route::get('availableStockDetails', function () {return view('stockHandelling/availableStock');});
Route::get('farmerProfile', function () {return view('farmerProfile/profile');});
Route::get('analysis', function () {return view('analysis/analysis');});
Route::get('riceCategory', function () {return view('riceCategory/rice');});
